School project with UE4, where you can trigger different shadings as you step onto the altars.

With 5 different altars we have 5 different animations, that happen. As you set
foot on the altar with blue fire, make sure to pay attention to what happens, so
that you don't break anything.

W - move forward,
A - strafe left,
D - strafe right,
S - move backward,
SPACE - jump,
Use mouse to look where you want to go,
ESC - exit the game.
